package ar.com.almundo;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.testng.Assert.assertTrue;

public class CallCenterTest {

    private CallCenter callCenter;
    private LocalDateTime startingTime;
    private List<Call> calls;
    private Integer totalEmployees;
    private Integer totalCalls;

    @BeforeMethod
    @Parameters({"calls", "operators", "supervisors", "directors"})
    public void setUp(Integer calls,
                      Integer operators, Integer supervisors, Integer directors) {
        callCenter = new CallCenter(operators, supervisors, directors);
        totalEmployees = operators + directors + supervisors;
        totalCalls = calls;
        this.calls = IntStream.range(0, calls).mapToObj(i -> new Call()).collect(Collectors.toList());
        startingTime = LocalDateTime.now();
    }

    @Test
    public void testIncomingCall() throws InterruptedException {
        calls.stream().parallel().forEach(call -> {
            try {
                callCenter.incomingCall(call);
            } catch (InterruptedException e) {
            }
        });
        while (!callCenter.employeesBored()) {
            TimeUnit.SECONDS.sleep(1);
        }
        LocalDateTime finalTime = LocalDateTime.now();
        Long time = startingTime.until(finalTime, ChronoUnit.SECONDS);
        Integer smallestSerialTime = totalCalls * 5;
        Integer biggestTimeParalelized = (totalCalls / totalEmployees) * 10;
        assertTrue(time < smallestSerialTime, "Is running serial");
        assertTrue(time <= biggestTimeParalelized, "It exists an event that is executing sequential and operating " +
                "as bottleneck");
    }
}