package ar.com.almundo;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public interface CallCenterEmployee {

    /**
     * Returns the rank of the employee among the company in ascending order
     *
     * @return the rank of the Employee
     */
    int rank();

    /**
     * Handles call. Default implementation will execute {@link Thread#sleep(long)} for a random time between
     * 5 and 10 seconds
     *
     * @param call the call to handle
     * @throws InterruptedException if execution is interrupted
     */
    default void handleCall(Call call) throws InterruptedException {
        TimeUnit.SECONDS.sleep(ThreadLocalRandom.current().nextInt(5, 10));
    }

}
