package ar.com.almundo;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.*;

public class CallCenterDispatcher {
    private ExecutorService executor;
    private BlockingQueue<Call> calls = new LinkedBlockingQueue<>();
    private BlockingQueue<CallCenterEmployee> employees;
    private Integer callCenterCapability;

    /**
     * Creates the dispatcher, assigns the employees and starts consumer thread.
     *
     * @param employees the list of employees.
     */
    public CallCenterDispatcher(List<CallCenterEmployee> employees) {
        this.employees = new PriorityBlockingQueue<>(employees.size(),
                Comparator.comparingInt(CallCenterEmployee::rank));
        callCenterCapability = employees.size();
        this.employees.addAll(employees);
        executor = Executors.newFixedThreadPool(callCenterCapability + 1);
        executor.execute(() -> {
            try {
                dispatchCalls();
            } catch (InterruptedException e) {
            }
        });
    }

    /**
     * This method will act as the consumer {@see <a href="https://en.wikipedia.org/wiki/Producer-consumer_problem">
     * Consumer-Product Algorithm</a>}, on a non-stop loop until it is interrupted. Internally, calls will be handled by
     * {@link CallCenterDispatcher#pickupCall()}, which will be using the blocking mechanism provided by {@link PriorityBlockingQueue}
     * for the employees and {@link LinkedBlockingQueue} for calls through {@link BlockingQueue#take()}.
     * Each call will be taken by a {@link CallCenterEmployee} on a separate thread. Check implementations for further
     * details about the specifics on how to handle the calls.
     *
     * @throws InterruptedException if execution is interrupted.
     */
    public void dispatchCalls() throws InterruptedException {
        while (true) {
            pickupCall();
        }
    }

    private void pickupCall() throws InterruptedException {
        Call call = calls.take();
        CallCenterEmployee employee = employees.take();
        executor.submit(() ->
        {
            try {
                employee.handleCall(call);
                employees.offer(employee);
            } catch (InterruptedException e) {
            }
        });
    }

    /**
     * Adds a call to the call queue and notifies any blocked execution through {@link LinkedBlockingQueue#offer(Object)}
     *
     * @param call the incoming call that will be added to the queue.
     */
    public void receiveCall(Call call) {
        calls.offer(call);
    }

    public Boolean noCalls() {
        return calls.isEmpty() && nobodyIsWorking();
    }

    /**
     * Checks if the current size of the employees queue is equal to the starting size of employees
     *
     * @return if they match.
     */
    private Boolean nobodyIsWorking() {
        return employees.size() == callCenterCapability;
    }

}
