package ar.com.almundo;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CallCenter {

    private CallCenterDispatcher dispatcher;
    private Integer totalEmployees;

    /**
     * Creates a call center based on the amount of different employees and starts the dispatcher.
     *
     * @param operators  amount of operators
     * @param supervisor amount of supervisors
     * @param directors  amount of directors
     */
    public CallCenter(Integer operators, Integer supervisor, Integer directors) {
        List<CallCenterEmployee> employees = IntStream.range(0, operators).mapToObj(i -> new PersonalJesus())
                .collect(Collectors.toList());
        employees.addAll(IntStream.range(0, directors).mapToObj(i -> new Director()).collect(Collectors.toList()));
        employees.addAll(IntStream.range(0, supervisor).mapToObj(i -> new Supervisor()).collect(Collectors.toList()));
        totalEmployees = employees.size();
        dispatcher = new CallCenterDispatcher(employees);
    }

    /**
     * Delegates call to dispatcher
     *
     * @param call
     * @throws InterruptedException
     */
    public void incomingCall(Call call) throws InterruptedException {
        dispatcher.receiveCall(call);
    }

    /**
     * Determines if no one is currently working due to lack of calls
     *
     * @return if there are no calls
     */
    public Boolean employeesBored() {
        return dispatcher.noCalls();
    }
}
